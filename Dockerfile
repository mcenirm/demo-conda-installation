FROM centos:7

ARG miniconda_user=conda
ARG miniconda_prefix=/usr/local/miniconda
ARG miniconda_installer_url=https://repo.anaconda.com/miniconda/Miniconda3-py38_4.8.3-Linux-x86_64.sh
ARG miniconda_installer_sha256=879457af6a0bf5b34b48c12de31d4df0ee2f06a8e68768e5758c3293b2daf688

RUN yum -q -y update \
 && yum -q -y clean all \
 && useradd --system ${miniconda_user} \
 && mkdir -v -- "${miniconda_prefix}" \
 && chown -c "${miniconda_user}:${miniconda_user}" -- "${miniconda_prefix}" \
 && chmod -c 0755 "${miniconda_prefix}"

USER ${miniconda_user}

WORKDIR ${miniconda_prefix}
RUN curl -sLR -o /tmp/mc3installer.sh -- "${miniconda_installer_url}" \
 && echo -e "#\n${miniconda_installer_sha256}  /tmp/mc3installer.sh" | sha256sum -c - \
 && bash /tmp/mc3installer.sh -b -f -p "${miniconda_prefix}" \
 && rm -v /tmp/mc3installer.sh \
 && bin/conda update --yes --quiet conda \
 && bin/conda clean  --yes --quiet --all \
 && chmod -R go-w,a+rX -- . \
 && find pkgs -mindepth 1 -maxdepth 1 -type d -execdir rmdir --ignore-fail-on-non-empty -- '{}' +
